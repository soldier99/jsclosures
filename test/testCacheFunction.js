const test = require('../cacheFunction');

let cb = ag =>{
    return `cb is invoked with argument ${ag}`;
}
let func = test(cb);
console.log(func('hi'));
console.log(func('hello'));
console.log(func('hi'));