
function limitFunctionCallCount(cb, n) {

    let counter = 1;

    let func = () =>{
        if(counter<=n){
            console.log(`the value of counter is ${counter}`);
            let k = cb();
            counter++;
            return k;
        }
        return null ;

        
    }
    
    return func ;

    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
}
module.exports = limitFunctionCallCount;