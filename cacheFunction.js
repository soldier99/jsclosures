function cacheFunction(cb) {

    let cache = {}


    let invokeCb = arg =>{
        let flag = false;
        for(let key in cache){
            if(key == arg){
                flag = true;
            }
        }
        if(flag){
            console.log('cached case found');
            return cache[arg];
        }
        cache[arg] = cb(arg);
        // console.log(cache)
        return cache[arg] ;
        
        
    }


    return invokeCb ;
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
}


module.exports = cacheFunction ;